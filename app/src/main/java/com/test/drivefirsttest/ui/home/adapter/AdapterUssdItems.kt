package com.test.drivefirsttest.ui.home.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.test.drivefirsttest.R
import com.test.drivefirsttest.data.local.entity.UssdEntity
import com.test.drivefirsttest.databinding.RowRecUssdItem2Binding
import com.test.drivefirsttest.util.Util
import java.util.*

class AdapterUssdItems(
    private val activity: AppCompatActivity,
) : RecyclerView.Adapter<AdapterUssdItems.UssdViewHolder>(),
    Filterable {
    private var ussdList: List<UssdEntity> = ArrayList()
    private var ussdListFiltered: List<UssdEntity> = ArrayList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        i: Int,
    ): UssdViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: RowRecUssdItem2Binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.row_rec_ussd_item_2,
            parent, false
        )
        return UssdViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: UssdViewHolder, position: Int,
    ) {
        val item = ussdListFiltered[position]
        holder.binding.item = item


        holder.binding.imgCall.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = ussdToCallableUri(
                ussdListFiltered[position].code
            )
            activity.startActivity(intent)
        }
        holder.binding.imgShare.setOnClickListener {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(
                Intent.EXTRA_TEXT, ussdListFiltered[position].name + "\n" +
                        ussdListFiltered[position].code + "\n" + " Ussd Code"
            )
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "The title")
            activity.startActivity(Intent.createChooser(shareIntent, "Share..."))
        }

        holder.binding.imgCopy.setOnClickListener {
            Util.copyToClipboard(activity, ussdListFiltered[position].code)
        }
    }

    override fun getItemCount(): Int {
        return ussdListFiltered.size
    }

    private fun ussdToCallableUri(ussd: String?): Uri {
        val uriString = StringBuilder()
        if (!ussd!!.startsWith("tel:")) uriString.append("tel:")
        for (c in ussd.toCharArray()) {
            if (c == '#') uriString.append(Uri.encode("#")) else uriString.append(c)
        }
        return Uri.parse(uriString.toString())
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(
                constraint: CharSequence,
                results: FilterResults,
            ) {
                ussdListFiltered = results.values as List<UssdEntity>
                notifyDataSetChanged()
            }

            override fun performFiltering(_constraint: CharSequence): FilterResults {
                var constraint = _constraint
                val results = FilterResults()
                val filteredArrList: MutableList<UssdEntity> =
                    ArrayList()
                if (constraint.isEmpty()) {
                    results.count = ussdList.size
                    results.values = ussdList
                } else {
                    constraint = constraint.toString().toLowerCase(Locale.ROOT)
                    for (i in ussdList.indices) {
                        val data: String = ussdList[i].name
                        if (constraint.toString().length == 1) {
                            if (data.toLowerCase(Locale.ROOT).startsWith(constraint.toString())) {
                                filteredArrList.add(
                                    UssdEntity(
                                        ussdList[i].id,
                                        ussdList[i].code,
                                        ussdList[i].name,
                                        ussdList[i].deletable
                                    )
                                )
                            }
                        } else if (data.toLowerCase(Locale.ROOT)
                                .contains(constraint.toString())
                        ) {
                            filteredArrList.add(
                                UssdEntity(
                                    ussdList[i].id,
                                    ussdList[i].code,
                                    ussdList[i].name,
                                    ussdList[i].deletable
                                )
                            )
                        }
                    }
                    results.count = filteredArrList.size
                    results.values = filteredArrList
                }
                return results
            }
        }
    }

    class UssdViewHolder(var binding: RowRecUssdItem2Binding) : ViewHolder(binding.root)

    fun setData(list: List<UssdEntity>) {
        ussdList = list
        ussdListFiltered = list
        notifyDataSetChanged()
    }

}