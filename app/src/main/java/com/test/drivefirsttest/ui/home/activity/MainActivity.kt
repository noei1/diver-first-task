package com.test.drivefirsttest.ui.home.activity

import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.ferfalk.simplesearchview.SimpleSearchView
import com.ferfalk.simplesearchview.SimpleSearchViewListener
import com.test.drivefirsttest.R
import com.test.drivefirsttest.databinding.ActivityMainBinding
import com.test.drivefirsttest.factory.ViewModelProviderFactory
import com.test.drivefirsttest.listener.OnFragmentListener
import com.test.drivefirsttest.listener.OnRecyclerShowListener
import com.test.drivefirsttest.listener.OnSearchListener
import com.test.drivefirsttest.util.SharedPref
import com.test.drivefirsttest.util.Utils
import dagger.android.support.DaggerAppCompatActivity
import me.ibrahimsn.lib.OnItemSelectedListener
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), View.OnClickListener {

    private lateinit var navController: NavController

    private var backPressToExit = 0L

    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    @Inject
    lateinit var sharedPref: SharedPref

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Utils.setStatusBarColor(this, ContextCompat.getColor(this, R.color.colorStatusBar))
        Utils.setLan(this, sharedPref)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initView()
    }

    private fun initView() {
        navController = findNavController(R.id.nav_host_fragment)

        binding.imgLeft.setOnClickListener(this)
        binding.imgRight.setOnClickListener(this)

        binding.txtHeader.typeface = Typeface.createFromAsset(assets, "fonts/ShortBaby.ttf")

        navController.navigate(R.id.frShowItems)

        binding.imgLeft.setOnClickListener(this)

        simpleSearchSetUp()
        listener()
    }

    private fun listener() {
        OnRecyclerShowListener.binder {
            if (it) {
                binding.bottomBar.animate()
                    .alpha(1f)
                    .setDuration(800)
                    .setListener(null)
                binding.bottomBar.visibility = View.VISIBLE
            } else {
                binding.bottomBar.animate()
                    .alpha(0f)
                    .setDuration(800)
                    .setListener(null)
                binding.bottomBar.visibility = View.GONE
            }


        }

        OnFragmentListener.binder {
            binding.multiSearch.closeSearch(true)
            when (navController.currentDestination!!.label) {

                "FrShowItems" -> {
                    binding.imgLeft.visibility = View.VISIBLE
                    binding.imgRight.visibility = View.INVISIBLE
                    binding.txtHeader.text = getString(R.string.txt_show)
                }

                "FrTransaction" -> {
                    binding.imgLeft.visibility = View.INVISIBLE
                    binding.imgRight.visibility = View.INVISIBLE
                    binding.txtHeader.text = getString(R.string.txt_transaction)
                }

                "FrSettings" -> {
                    binding.imgLeft.visibility = View.INVISIBLE
                    binding.imgRight.visibility = View.INVISIBLE
                    binding.txtHeader.text = getString(R.string.txt_settings)
                }

            }
        }

        binding.bottomBar.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelect(pos: Int): Boolean {
                when (pos) {
                    0 -> {
                        navController.navigate(R.id.frShowItems)
                    }
                    1 -> {
                        navController.navigate(R.id.frTransaction)
                    }
                    2 -> {
                        navController.navigate(R.id.frSettings)
                    }
                }

                return true
            }

        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.img_left -> binding.multiSearch.showSearch()
        }
    }

    override fun onBackPressed() {
        when (navController.currentDestination!!.label) {

            "FrShowItems" -> {
                exitApp()
            }

            "FrTransaction" -> {
                navController.navigate(R.id.frShowItems)
                binding.bottomBar.itemActiveIndex = 0
            }

            "FrSettings" -> {
                navController.navigate(R.id.frShowItems)
                binding.bottomBar.itemActiveIndex = 0

            }

        }
    }

    private fun exitApp() {
        if (backPressToExit + 2000 > System.currentTimeMillis()) {
            finishAffinity()
        } else {
            Toast.makeText(this, R.string.click_back, Toast.LENGTH_SHORT).show()
        }
        backPressToExit = System.currentTimeMillis()
    }

    private fun simpleSearchSetUp() {
        binding.multiSearch.setOnQueryTextListener(object : SimpleSearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean {
                OnSearchListener.onSearch(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                OnSearchListener.onSearch(newText)

                return false
            }

            override fun onQueryTextCleared(): Boolean {
                return false
            }

        })

        binding.multiSearch.setOnSearchViewListener(object : SimpleSearchViewListener() {

            override fun onSearchViewShown() {
                super.onSearchViewShown()
                binding.imgLeft.visibility = View.INVISIBLE
                binding.imgRight.visibility = View.INVISIBLE
                binding.txtHeader.visibility = View.INVISIBLE
            }

            override fun onSearchViewClosed() {
                super.onSearchViewClosed()
                binding.imgLeft.visibility = View.VISIBLE
                binding.imgRight.visibility = View.VISIBLE
                binding.txtHeader.visibility = View.VISIBLE
            }

        })
    }

}