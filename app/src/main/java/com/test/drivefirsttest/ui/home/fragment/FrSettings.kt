package com.test.drivefirsttest.ui.home.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.test.drivefirsttest.R
import com.test.drivefirsttest.databinding.FrSettingsBinding
import com.test.drivefirsttest.listener.OnFragmentListener
import com.test.drivefirsttest.model.PanelItem
import com.test.drivefirsttest.ui.home.adapter.AdapterSettings
import com.test.drivefirsttest.util.SharedPref
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class FrSettings : DaggerFragment() {

    @Inject
    lateinit var sharedPref: SharedPref

    private val data: MutableList<PanelItem> = mutableListOf()
    private lateinit var adapterNewHouse: AdapterSettings
    private lateinit var navController: NavController
    private lateinit var binding: FrSettingsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fr_settings, container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val animation = AnimationUtils.loadLayoutAnimation(
            requireActivity(),
            R.anim.layout_animation_up_to_down
        )
        OnFragmentListener.onChanged()

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)

        adapterNewHouse = AdapterSettings(requireActivity(), navController)
        binding.recyclerViewPanel.layoutAnimation = animation
        binding.recyclerViewPanel.adapter = adapterNewHouse

        populateNewAdapter()
    }

    private fun populateNewAdapter() {
        data.add(
            PanelItem(
                0,
                "Profile",
                R.drawable.ic_user
            )
        )
        data.add(
            PanelItem(
                1,
                "Share",
                R.drawable.ic_share_colorful
            )
        )
        data.add(
            PanelItem(
                2,
                "About us",
                R.drawable.ic_information
            )
        )
        data.add(
            PanelItem(
                3,
                "Log Out",
                R.drawable.ic_log_out
            )
        )
        adapterNewHouse.addData(data)
    }

}