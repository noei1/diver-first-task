package com.test.drivefirsttest.ui.home.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.test.drivefirsttest.data.local.entity.UssdEntity;
import com.test.drivefirsttest.data.repository.TransactionRepository;
import com.test.drivefirsttest.data.repository.UssdRepository;
import com.test.drivefirsttest.network.OnResponseListener;

import java.util.List;

import javax.inject.Inject;

public class MainViewModel extends ViewModel {
    private final UssdRepository ussdRepository;
    private final TransactionRepository transactionRepository;

    @Inject
    public MainViewModel(UssdRepository ussdRepository, TransactionRepository transactionRepository) {
        this.ussdRepository = ussdRepository;
        this.transactionRepository = transactionRepository;
    }

    public LiveData<List<UssdEntity>> getAllUssdList() {
        return ussdRepository.getAllEntity();
    }

    public LiveData<List<UssdEntity>> getUssdList(int idCategory) {
        return ussdRepository.getUssdList(idCategory);
    }

    public void insertUssd(UssdEntity ussdEntity) {
        ussdRepository.addUssd(ussdEntity);
    }

    public void getTransaction(OnResponseListener listener) {
        transactionRepository.getPhoto(listener);
    }

}