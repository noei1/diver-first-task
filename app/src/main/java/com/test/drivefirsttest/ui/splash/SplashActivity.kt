package com.test.drivefirsttest.ui.splash

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.test.drivefirsttest.R
import com.test.drivefirsttest.databinding.ActivitySplashBinding
import com.test.drivefirsttest.ui.home.activity.MainActivity
import com.test.drivefirsttest.util.SharedPref
import com.test.drivefirsttest.util.Utils
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashActivity : DaggerAppCompatActivity() {
    private val timeOut: Long = 5000

    @Inject
    lateinit var sharedPref: SharedPref

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Utils.hideStatusBar(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        binding.textView5.typeface = Typeface.createFromAsset(assets, "fonts/ShortBaby.ttf")

        try {
            val pInfo = packageManager.getPackageInfo(packageName, 0)
            val version = pInfo.versionName
            binding.txtVersion.text = "Version : $version"
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        CoroutineScope(Main).launch {
            delay(timeOut)
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }

    }

}