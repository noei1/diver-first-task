package com.test.drivefirsttest.ui.home.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.drivefirsttest.R
import com.test.drivefirsttest.databinding.FrShowBinding
import com.test.drivefirsttest.factory.ViewModelProviderFactory
import com.test.drivefirsttest.listener.OnFragmentListener
import com.test.drivefirsttest.listener.OnRecyclerShowListener
import com.test.drivefirsttest.listener.OnSearchListener
import com.test.drivefirsttest.listener.OnSearchedText
import com.test.drivefirsttest.ui.home.adapter.AdapterUssdItems
import com.test.drivefirsttest.ui.home.viewmodel.MainViewModel
import com.test.drivefirsttest.util.SharedPref
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class FrShowItems : DaggerFragment(), OnSearchedText {
    private lateinit var adapterUssdItems: AdapterUssdItems
    private lateinit var binding: FrShowBinding

    private lateinit var viewModel: MainViewModel

    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    @Inject
    lateinit var sharedPref: SharedPref

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fr_show, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        OnFragmentListener.onChanged()

        adapterUssdItems = AdapterUssdItems(activity as AppCompatActivity)

        binding.recyclerCategory.layoutManager = LinearLayoutManager(activity)
        binding.recyclerCategory.adapter = adapterUssdItems
        binding.recyclerCategory.layoutAnimation =
            AnimationUtils.loadLayoutAnimation(requireContext(), R.anim.layout_animation_up_to_down)

        viewModel = ViewModelProvider(this, providerFactory).get(MainViewModel::class.java)

        viewModel.allUssdList.observe(requireActivity(), {
            adapterUssdItems.setData(it)
        })

        OnSearchListener.binder {
            adapterUssdItems.filter.filter(it.toString())
        }

        binding.recyclerCategory.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                when {
                    dy <= 0 -> {
                        // Show
                        OnRecyclerShowListener.onShow(true);
                    }
                    else -> {
                        // Hide
                        OnRecyclerShowListener.onShow(false);
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }

    override fun onText(charSequence: CharSequence?) {
        adapterUssdItems.filter.filter(charSequence.toString())
    }

}