package com.test.drivefirsttest.ui.home.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.test.drivefirsttest.R
import com.test.drivefirsttest.databinding.FrTransactionBinding
import com.test.drivefirsttest.factory.ViewModelProviderFactory
import com.test.drivefirsttest.listener.OnFragmentListener
import com.test.drivefirsttest.listener.OnRecyclerShowListener
import com.test.drivefirsttest.network.OnResponseListener
import com.test.drivefirsttest.network.transaction.model.RetTransaction
import com.test.drivefirsttest.ui.home.adapter.AdapterTransaction
import com.test.drivefirsttest.ui.home.viewmodel.MainViewModel
import com.test.drivefirsttest.util.SharedPref
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class FrTransaction : DaggerFragment(), OnResponseListener {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: AdapterTransaction
    private lateinit var navController: NavController
    private lateinit var binding: FrTransactionBinding

    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    @Inject
    lateinit var sharedPref: SharedPref

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fr_transaction, container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        OnFragmentListener.onChanged()
        navController = findNavController(this)

        binding.recyclerTransaction.layoutAnimation =
            AnimationUtils.loadLayoutAnimation(requireContext(), R.anim.layout_animation_up_to_down)

        adapter = AdapterTransaction()
        binding.recyclerTransaction.adapter = adapter

        viewModel = ViewModelProvider(this, providerFactory).get(MainViewModel::class.java)
        viewModel.getTransaction(this)

        binding.recyclerTransaction.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                when {
                    dy <= 0 -> {
                        // Show
                        OnRecyclerShowListener.onShow(true);
                    }
                    else -> {
                        // Hide
                        OnRecyclerShowListener.onShow(false);
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }

    override fun onSuccessResponse(o: Any?) {
        Log.e("Fragment", "onSuccessResponse: $o")
        val response = o as MutableList<RetTransaction>

        adapter.addData(o)
    }

    override fun onErrorCodeResponse(errorCode: Int, errorMsg: String?) {

    }

    override fun onFailure(msg: String?) {
    }

    override fun inProgress() {
        binding.recyclerTransaction.visibility = View.INVISIBLE
        binding.progress.visibility = View.VISIBLE
    }

    override fun outProgress() {
        binding.recyclerTransaction.visibility = View.VISIBLE
        binding.progress.visibility = View.GONE
    }

}