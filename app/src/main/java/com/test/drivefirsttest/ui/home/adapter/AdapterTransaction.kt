package com.test.drivefirsttest.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.drivefirsttest.R
import com.test.drivefirsttest.databinding.RowRecTransactionBinding
import com.test.drivefirsttest.network.transaction.model.RetTransaction

class AdapterTransaction : RecyclerView.Adapter<AdapterTransaction.TransactionViewHolder>() {
    private var list: MutableList<RetTransaction> = mutableListOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        i: Int
    ): TransactionViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: RowRecTransactionBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.row_rec_transaction,
            parent, false
        )
        return TransactionViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: TransactionViewHolder,
        position: Int
    ) {
        val item = list[position]
        holder.binding.item = item
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class TransactionViewHolder(val binding: RowRecTransactionBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun addData(dataViews: MutableList<RetTransaction>) {
        this.list.addAll(dataViews)
        notifyDataSetChanged()
    }

}