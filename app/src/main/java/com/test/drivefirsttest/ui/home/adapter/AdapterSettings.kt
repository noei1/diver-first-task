package com.test.drivefirsttest.ui.home.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.test.drivefirsttest.R
import com.test.drivefirsttest.databinding.RowRecSettingsBinding
import com.test.drivefirsttest.model.PanelItem

class AdapterSettings(
    private val activity: Activity,
    var navController: NavController,
) : RecyclerView.Adapter<AdapterSettings.PanelViewHolder>() {
    private var list: MutableList<PanelItem> = mutableListOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        i: Int,
    ): PanelViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: RowRecSettingsBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.row_rec_settings,
            parent, false
        )
        return PanelViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: PanelViewHolder, position: Int,
    ) {
        val item = list[position]

        holder.binding.panel = item
        holder.binding.cardView.setOnClickListener {
            when (item.id) {
                0 -> {
                }

                1 -> {

                }

                2 -> {
                }

                3 -> {
                }
            }
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    class PanelViewHolder(val binding: RowRecSettingsBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun addData(dataViews: MutableList<PanelItem>) {
        this.list.addAll(dataViews)
        notifyDataSetChanged()
    }

}