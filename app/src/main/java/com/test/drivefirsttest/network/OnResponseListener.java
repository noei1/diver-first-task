package com.test.drivefirsttest.network;

public interface OnResponseListener {

    void onSuccessResponse(Object o);

    void onErrorCodeResponse(int errorCode, String errorMsg);

    void onFailure(String msg);

    void inProgress();

    void outProgress();

}
