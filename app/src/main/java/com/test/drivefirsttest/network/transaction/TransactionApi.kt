package com.test.drivefirsttest.network.transaction

import com.test.drivefirsttest.network.transaction.model.RetTransaction
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface TransactionApi {

    @Headers("Accept: application/json", "Content-Type: application/json")
    @GET("photos")
    fun getPhoto(): Call<MutableList<RetTransaction>>

}