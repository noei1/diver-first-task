package com.test.drivefirsttest.di.activity.splash

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class SplashScope {}