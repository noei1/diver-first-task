package com.test.drivefirsttest.di.activity.main

import androidx.lifecycle.ViewModel
import com.test.drivefirsttest.di.ViewModelKey
import com.test.drivefirsttest.ui.home.viewmodel.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindViewViewModel(viewModel: MainViewModel): ViewModel

}