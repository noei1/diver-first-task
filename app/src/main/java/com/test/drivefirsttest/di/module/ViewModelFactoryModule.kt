package com.test.drivefirsttest.di.module

import androidx.lifecycle.ViewModelProvider
import com.test.drivefirsttest.factory.ViewModelProviderFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelProviderFactory): ViewModelProvider.Factory

}