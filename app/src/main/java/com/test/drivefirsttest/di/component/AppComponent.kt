package com.test.drivefirsttest.di.component

import android.app.Application
import com.test.drivefirsttest.BaseApplication
import com.test.drivefirsttest.di.module.ActivityBuildersModule
import com.test.drivefirsttest.di.module.AppModule
import com.test.drivefirsttest.di.module.RoomDbModule
import com.test.drivefirsttest.di.module.ViewModelFactoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class, ActivityBuildersModule::class,
        AppModule::class, ViewModelFactoryModule::class, RoomDbModule::class]
)
interface AppComponent : AndroidInjector<BaseApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }

}