package com.test.drivefirsttest.di.activity.main

import com.test.drivefirsttest.ui.home.fragment.FrSettings
import com.test.drivefirsttest.ui.home.fragment.FrShowItems
import com.test.drivefirsttest.ui.home.fragment.FrTransaction
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeShowFragment(): FrShowItems?

    @ContributesAndroidInjector
    abstract fun contributeSettingsFragment(): FrSettings?

    @ContributesAndroidInjector
    abstract fun contributeTransactionFragment(): FrTransaction?

}