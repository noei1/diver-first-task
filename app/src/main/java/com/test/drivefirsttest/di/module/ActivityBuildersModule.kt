package com.test.drivefirsttest.di.module

import com.test.drivefirsttest.di.activity.main.MainFragmentBuildersModule
import com.test.drivefirsttest.di.activity.main.MainModule
import com.test.drivefirsttest.di.activity.main.MainScope
import com.test.drivefirsttest.di.activity.main.MainViewModelModule
import com.test.drivefirsttest.di.activity.splash.SplashScope
import com.test.drivefirsttest.ui.home.activity.MainActivity
import com.test.drivefirsttest.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @SplashScope
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @MainScope
    @ContributesAndroidInjector(
        modules = [MainModule::class, MainViewModelModule::class, MainFragmentBuildersModule::class]
    )
    abstract fun contributeMainActivity(): MainActivity


}