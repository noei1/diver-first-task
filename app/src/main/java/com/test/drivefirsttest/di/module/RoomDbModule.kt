package com.test.drivefirsttest.di.module

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.test.drivefirsttest.data.local.AppDatabase
import com.test.drivefirsttest.data.local.dao.UssdDao
import com.test.drivefirsttest.data.local.entity.UssdEntity
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Singleton

@Module
class RoomDbModule {
    private lateinit var appDatabase: AppDatabase
    private lateinit var codeApplication: Application

    @Singleton
    @Provides
    fun getRoomDatabase(application: Application): AppDatabase {
        codeApplication = application

        appDatabase = Room.databaseBuilder(
            codeApplication, AppDatabase::class.java, "diver_test.db"
        )
            .allowMainThreadQueries()
            .setJournalMode(RoomDatabase.JournalMode.TRUNCATE)
            .fallbackToDestructiveMigration()
            .addCallback(rdc)
            .build()
        return appDatabase
    }

    @Singleton
    @Provides
    fun provideUssdDao(roomDatabase: AppDatabase) = roomDatabase.dbUssd()

    private var rdc = object : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            CoroutineScope(Dispatchers.IO).launch {
                prePopulateData()
            }
        }

    }

    private fun prePopulateData() {
        val daoUssd = appDatabase.dbUssd()

        daoUssd.deleteAll()

        // Ussd
        addAndroidUssd(daoUssd)
    }

    // USSD Codes
    private fun addAndroidUssd(daoUssd: UssdDao) {
        val idSubCategory = 1

        val item1 =
            UssdEntity(idSubCategory, "*#*#4636#*#*", "About Phone", false)

        val item2 =
            UssdEntity(idSubCategory, "*#*#4636#*#*", "About Phone", false)
        val item3 =
            UssdEntity(idSubCategory, "*#*#7780#*#*", "Restore Factory Settings", false)

        val item4 =
            UssdEntity(
                idSubCategory,
                "*2767*3855#",
                "Format Device to factory state",
                false
            )

        val item5 =
            UssdEntity(idSubCategory, "*2767*226372#", "Format soft reset", false)
        val item6 =
            UssdEntity(idSubCategory, "*#*#34971539#*#*", "About Camera", false)

        val item7 =
            UssdEntity(
                idSubCategory, "*#*#7594#*#*",
                "Change the Power button Behavior - enables Direct PowerOff",
                false
            )
        val item8 =
            UssdEntity(
                idSubCategory,
                "*#*#273283*255*663282*#*#*",
                "Backup Media Files",
                false
            )
        val item9 =
            UssdEntity(
                idSubCategory,
                "*#*#197328640#*#*", "Enabling Test Mode for Service Activity",
                false
            )
        val item10 =
            UssdEntity(idSubCategory, "*#*#232339#*#*", "Wireless Lan Test", false)
        val item11 =
            UssdEntity(idSubCategory, "*#*#526#*#*", "Wireless Lan Test", false)
        val item12 =
            UssdEntity(idSubCategory, "*#*#528#*#*", "Wireless Lan Test", false)
        val item13 =
            UssdEntity(
                idSubCategory,
                "*#*#232338#*#*",
                "Displays Wi-Fi Mac-address",
                false
            )
        val item14 =
            UssdEntity(idSubCategory, "*#*#1472365#*#*", "GPS Test", false)
        val item15 =
            UssdEntity(idSubCategory, "*#*#1575#*#*", "Different GPS Test", false)
        val item16 =
            UssdEntity(idSubCategory, "*#*#0283#*#*", "Packet Loopback Test", false)
        val item17 =
            UssdEntity(idSubCategory, "*#*#0*#*#*", "LCD Display Test", false)
        val item18 =
            UssdEntity(idSubCategory, "*#*#0673#*#*", "Audio Test", false)
        val item19 =
            UssdEntity(idSubCategory, "*#*#0289#*#*", "Audio Test", false)
        val item20 =
            UssdEntity(
                idSubCategory,
                "*#*#0842#*#*",
                "Vibration and Back-light Test",
                false
            )
        val item21 =
            UssdEntity(
                idSubCategory,
                "*#*#2663#*#*",
                "Displays Touch-Screen Version",
                false
            )
        val item22 =
            UssdEntity(idSubCategory, "*#*#2664#*#*", "Touch-Screen Test", false)
        val item23 =
            UssdEntity(idSubCategory, "*#*#0588#*#*", "Proximity Sensor Test", false)
        val item24 =
            UssdEntity(idSubCategory, "*#*#3264#*#*", "Ram version", false)
        val item25 =
            UssdEntity(idSubCategory, "*#*#232331#*#*", "Bluetooth Test", false)
        val item26 =
            UssdEntity(idSubCategory, "*#*#7262626#*#*", "Field Test", false)
        val item27 =
            UssdEntity(
                idSubCategory,
                "*#*#232337#*#*",
                "Displays Bluetooth Device Address",
                false
            )
        val item28 =
            UssdEntity(idSubCategory, "*#*#8255#*#*", "G-Talk Monitoring", false)
        val item29 =
            UssdEntity(
                idSubCategory, "*#*#4986*2650468#*#*",
                "PDA, Phone, Hardware, RF Call Date Firmware info",
                false
            )
        val item30 =
            UssdEntity(idSubCategory, "*#*#1234#*#*", "PDA, Phone Firmware info", false)
        val item31 =
            UssdEntity(idSubCategory, "*#*#1111#*#*", "FTA Software Version", false)
        val item32 =
            UssdEntity(idSubCategory, "*#*#2222#*#*", "FTA Hardware Version", false)
        val item33 =
            UssdEntity(
                idSubCategory, "*#*#44336#*#*",
                "Displays Build Time and Change List Number",
                false
            )
        val item34 =
            UssdEntity(idSubCategory, "*#06#", "IMEI Number", false)
        val item35 =
            UssdEntity(
                idSubCategory,
                "*#*#8351#*#*",
                "Enable Voice Dialing Logging Mode",
                false
            )
        val item36 =
            UssdEntity(
                idSubCategory,
                "*#*#8350#*#*",
                "Disable Voice Dialing Logging Mode",
                false
            )
        val item37 =
            UssdEntity(
                idSubCategory, "**05***#",
                "Execute From Emergency Dial Screen to Unlock OUK Code",
                false
            )
        val item38 =
            UssdEntity(idSubCategory, "*#872564#", "USB logging control", false)
        val item39 =
            UssdEntity(idSubCategory, "*#9090#", "Diagnostic configuration", false)
        val item40 =
            UssdEntity(idSubCategory, "*#301279#", "HSDPA/HSUPA Control Menu", false)
        val item41 =
            UssdEntity(idSubCategory, "*#9900#", "System dump mode", false)
        val item42 =
            UssdEntity(idSubCategory, "*#7465625#", "Access phone lock status", false)
        val item43 = UssdEntity(
            idSubCategory,
            "*#12580*369#",
            "Software and Hardware details",
            false
        )
        val item44 =
            UssdEntity(
                idSubCategory,
                "*#*#8255#*#*",
                "Google Talk service monitoring",
                false
            )
        val item45 =
            UssdEntity(
                idSubCategory,
                "*#*#8255#*#*",
                "Software and Hardware details",
                false
            )
        val item46 =
            UssdEntity(idSubCategory, "*#272886#", "Android Answer Selection", false)
        val item47 =
            UssdEntity(idSubCategory, "*#9998*246#", "Battery status", false)
        val item48 =
            UssdEntity(idSubCategory, "*#2255#", "Call", false)
        val item49 =
            UssdEntity(
                idSubCategory,
                "*#273283*255*663282*#",
                "Data Create SD Card",
                false
            )
        val item50 =
            UssdEntity(idSubCategory, "*#9998*324#", "Debug screen", false)
        val item51 =
            UssdEntity(idSubCategory, "*#8999*324#", "E2P Camera Reset", false)
        val item52 =
            UssdEntity(idSubCategory, "*#4238378#", "GPS test settings", false)
        val item53 =
            UssdEntity(idSubCategory, "*#*#1472365#*#*", "GPS test settings", false)
        val item54 =
            UssdEntity(idSubCategory, "*#0*#", "General Test Mode", false)
        val item55 =
            UssdEntity(idSubCategory, "#*1400#", "IMSI", false)
        val item56 =
            UssdEntity(
                idSubCategory,
                "*7465625*27#",
                "Insert Network Lock Keycode NSP/CP",
                false
            )
        val item57 =
            UssdEntity(
                idSubCategory,
                "#7465625*27#",
                "Insert Content Provider Keycode",
                false
            )
        val item58 =
            UssdEntity(
                idSubCategory, "#4765625*782#",
                "Insert Partial Network Lock Keycode",
                false
            )
        val item59 =
            UssdEntity(idSubCategory, "*#8999*523#", "LCD Brightness", false)
        val item60 =
            UssdEntity(idSubCategory, "*#3214789650#", "LBS Test Mode", false)
        val item61 =
            UssdEntity(idSubCategory, "*#0782#", "Real Time Clock Test", false)
        val item62 =
            UssdEntity(idSubCategory, "*#0001#", "Serial Number", false)
        val item63 =
            UssdEntity(idSubCategory, "*#07#", "Test History", false)
        val item64 =
            UssdEntity(idSubCategory, "*2767*73738927#", "Test History", false)

        daoUssd.insertAll(
            item1,
            item2,
            item3,
            item4,
            item5,
            item6,
            item7,
            item8,
            item9,
            item10,
            item11,
            item12,
            item13,
            item14,
            item15,
            item16,
            item17,
            item18,
            item19,
            item20,
            item21,
            item22,
            item23,
            item24,
            item25,
            item26,
            item27,
            item28,
            item29,
            item30,
            item31,
            item32,
            item33,
            item34,
            item35,
            item36,
            item37,
            item38,
            item39,
            item40,
            item41,
            item42,
            item43,
            item44,
            item45,
            item46,
            item47,
            item48,
            item49,
            item50,
            item51,
            item52,
            item53,
            item54,
            item55,
            item56,
            item57,
            item58,
            item59,
            item60,
            item61,
            item62,
            item63,
            item64
        )

    }

}