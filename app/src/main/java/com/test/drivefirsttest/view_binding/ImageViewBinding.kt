package com.test.drivefirsttest.view_binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.rishabhharit.roundedimageview.RoundedImageView
import com.squareup.picasso.Picasso

@BindingAdapter("android:loadImage")
fun loadImage(imageView: ImageView, imageUrl: Int) {
    imageView.setImageResource(imageUrl)
}

@BindingAdapter("android:loadImageURL")
fun loadImage(imageView: RoundedImageView, imageUrl: String?) {
    Picasso.get().load(imageUrl).into(imageView)
}
