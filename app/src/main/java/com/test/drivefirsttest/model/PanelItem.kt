package com.test.drivefirsttest.model

class PanelItem(val id: Int, val title: String, val iconResource: Int)