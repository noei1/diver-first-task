package com.test.drivefirsttest.listener;

public interface OnSearch {
    void onSearch(CharSequence character);
}
