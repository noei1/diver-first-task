package com.test.drivefirsttest.listener;

public class OnRecyclerShowListener {
    private static OnRecyclerScrolled onSearchListener;

    public static void binder(OnRecyclerScrolled settings) {
        onSearchListener = settings;
    }

    public static void onShow(Boolean show) {
        if (onSearchListener != null) onSearchListener.onShow(show);
    }

}