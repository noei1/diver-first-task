package com.test.drivefirsttest.listener;

public interface OnRecyclerScrolled {
    void onShow(Boolean show);
}