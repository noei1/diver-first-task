package com.test.drivefirsttest.listener;

public class OnFragmentListener {
    private static OnFragmentChanged onFragmentListener;

    public static void binder(OnFragmentChanged listener) {
        onFragmentListener = listener;
    }

    public static void onChanged() {
        if (onFragmentListener != null) {
            onFragmentListener.onChanged();
        }
    }

}