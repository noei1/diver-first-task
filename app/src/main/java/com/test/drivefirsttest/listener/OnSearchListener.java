package com.test.drivefirsttest.listener;

public class OnSearchListener {
    private static OnSearch onSearch;

    public static void binder(OnSearch search) {
        onSearch = search;
    }

    public static void onSearch(CharSequence character) {
        if (onSearch != null) {
            onSearch.onSearch(character);
        }
    }

}
