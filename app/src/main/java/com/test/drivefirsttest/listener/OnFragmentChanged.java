package com.test.drivefirsttest.listener;

public interface OnFragmentChanged {
    void onChanged();
}