package com.test.drivefirsttest.listener

interface OnSearchedText {
    fun onText(charSequence: CharSequence?)
}