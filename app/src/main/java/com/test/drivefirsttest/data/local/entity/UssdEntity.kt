package com.test.drivefirsttest.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ussd")
data class UssdEntity(
    var idSubCategory: Int,
    var code: String,
    var name: String,
    var deletable: Boolean
) {
    @PrimaryKey(autoGenerate = true)
    var id = 0

}