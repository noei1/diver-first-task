package com.test.drivefirsttest.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.test.drivefirsttest.data.local.entity.UssdEntity

@Dao
interface UssdDao {
    @Query("SELECT * FROM ussd")
    fun getAll(): LiveData<List<UssdEntity>>

    @Query("SELECT * FROM ussd WHERE idSubCategory=:idSubCategory")
    fun getAllSubItem(idSubCategory: Int): LiveData<List<UssdEntity>>

    @Query("SELECT * FROM ussd WHERE code=:code AND name=:name")
    fun getUssd(code: String, name: String): UssdEntity

    @Query("SELECT * FROM ussd WHERE id=:id")
    fun getUssd(id: Int): UssdEntity

    @Query("DELETE FROM ussd")
    fun deleteAll()

    @Insert
    fun insertAll(vararg clsUssd: UssdEntity)

    @Delete
    fun delete(vararg clsUssd: UssdEntity?)

}