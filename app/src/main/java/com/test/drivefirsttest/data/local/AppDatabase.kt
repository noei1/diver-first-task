package com.test.drivefirsttest.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.drivefirsttest.data.local.dao.UssdDao
import com.test.drivefirsttest.data.local.entity.UssdEntity

@Database(
    entities = [UssdEntity::class],
    version = 3, exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun dbUssd(): UssdDao
}