package com.test.drivefirsttest.data.repository

import com.test.drivefirsttest.network.OnResponseListener
import com.test.drivefirsttest.network.transaction.TransactionApi
import com.test.drivefirsttest.network.transaction.model.RetTransaction
import com.test.drivefirsttest.util.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TransactionRepository @Inject constructor(var api: TransactionApi) {
    private lateinit var listener: OnResponseListener

    fun getPhoto(listener: OnResponseListener) {
        this.listener = listener
        listener.inProgress()
        try {
            val call1: Call<MutableList<RetTransaction>> = api.getPhoto()
            call1.enqueue(object : Callback<MutableList<RetTransaction>> {
                override fun onResponse(
                    call: Call<MutableList<RetTransaction>>,
                    response: Response<MutableList<RetTransaction>>,
                ) {
                    listener.outProgress()

                    if (response.isSuccessful) {
                        listener.onSuccessResponse(response.body())
                    } else {
                        listener.onErrorCodeResponse(
                            response.code(),
                            Utils.getErrorMessage(
                                response.errorBody()!!.string()
                            )
                        )
                    }
                }

                override fun onFailure(call: Call<MutableList<RetTransaction>>, t: Throwable) {
                    listener.outProgress()
                    listener.onFailure(if (t is IOException) "this is an actual network failure :( inform the user and possibly retry" else "Conversion issue! big problems :(")
                }
            })
        } catch (e: Exception) {
            listener.outProgress()
            listener.onFailure("خطا در عملکرد")
        }
    }

}