package com.test.drivefirsttest.data.repository

import androidx.lifecycle.LiveData

import com.test.drivefirsttest.data.local.AppDatabase
import com.test.drivefirsttest.data.local.dao.UssdDao
import com.test.drivefirsttest.data.local.entity.UssdEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.system.measureNanoTime

@Singleton
class UssdRepository @Inject constructor(ussdDao: UssdDao, appDatabase: AppDatabase) {
    private var appDatabase: AppDatabase? = null
    private var ussdDao: UssdDao? = null

    var allEntity: LiveData<List<UssdEntity>>

    init {
        this.appDatabase = appDatabase
        this.ussdDao = appDatabase.dbUssd()
        allEntity = ussdDao.getAll()
    }

    fun addUssd(ussdEntity: UssdEntity) {
        CoroutineScope(IO).launch {
            val time = measureNanoTime {
                ussdDao!!.insertAll(ussdEntity)
            }

            println("Time to add Ussd: $time ")
        }
    }

    fun deleteUssd() {

    }

    fun getUssdList(categoryId: Int): LiveData<List<UssdEntity>> {
        return ussdDao!!.getAllSubItem(categoryId)
    }

}