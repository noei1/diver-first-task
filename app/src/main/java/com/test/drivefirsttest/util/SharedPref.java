package com.test.drivefirsttest.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPref {
    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;

    public SharedPref(Context context) {
        sharedPreferences = context.getSharedPreferences("Pref", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.apply();
    }

    public String getString(String Key, String defaultValue) {
        return sharedPreferences.getString(Key, defaultValue);
    }

    public int getInt(String Key, int defaultValue) {
        return sharedPreferences.getInt(Key, defaultValue);
    }

    public Boolean getBoolean(String Key, Boolean defaultValue) {
        return sharedPreferences.getBoolean(Key, defaultValue);
    }

    public long getLong(String Key, long defaultValue) {
        return sharedPreferences.getLong(Key, defaultValue);
    }

    public void setString(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public void setInt(String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }

    public void setBoolean(String key, Boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void setLong(String key, long value) {
        editor.putLong(key, value);
        editor.apply();
    }

    public void clear() {
        editor.clear();
        editor.apply();
    }

}
