package com.test.drivefirsttest.util

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Build
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.test.drivefirsttest.R
import java.util.*

class Util {

    companion object {

        fun getAndroidVersion(): String {
            return Build.VERSION.RELEASE + " " + Build.VERSION.CODENAME + " " + Build.VERSION.SDK_INT +
                    " " + " " + Build.MODEL
        }

        fun isPermissionGranted(
            context: Context,
            permission: String,
        ): Boolean {
            return context.checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
        }

        fun loadFragment(
            activity: AppCompatActivity,
            frameHolderId: Int,
            fragment: Fragment?,
            tag: String?,
        ) {
            try {
                if (!activity.isFinishing && !activity.isDestroyed) {
                    val fm: FragmentManager = activity.supportFragmentManager
                    val fragmentTransaction: FragmentTransaction = fm.beginTransaction()
                    fragment?.let {
                        fragmentTransaction.replace(frameHolderId, it, tag)
                            .addToBackStack(tag)
                            .commit()
                    }
                }
            } catch (e: Exception) {
                Log.e("TAG", "Ex =>$e")
            }
        }

        fun setInitials(initials: AppCompatActivity) {
            setStatusBarColor(initials, R.color.white)
            initials.supportActionBar!!.hide()
        }

        private fun setStatusBarColor(activity: Activity, color: Int) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                activity.window.statusBarColor = ContextCompat.getColor(activity, color)
            } else {
                val window: Window = activity.window
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                    window.statusBarColor = ContextCompat.getColor(activity, color)
                }
            }
        }

        fun copyToClipboard(
            activity: AppCompatActivity,
            textToCopy: String,
        ) {
            val clipboardManager =
                activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("ussd_code", textToCopy)
            clipboardManager.setPrimaryClip(clipData)
            Toast.makeText(activity, "Copied!", Toast.LENGTH_SHORT).show()
        }

        fun setLan(activity: AppCompatActivity, sharedPref: SharedPref) {
            val languageToLoad: String =
                if (sharedPref.getBoolean(Constants.USER_LANGUAGE, false)) "en" else "fa"
            val locale = Locale(languageToLoad)
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            activity.resources.updateConfiguration(config, activity.resources.displayMetrics)
            applyTheme(activity)
        }

        private fun applyTheme(activity: AppCompatActivity?) {
            val sharedPref = SharedPref(activity!!)
            AppCompatDelegate.setDefaultNightMode(
                if (sharedPref.getBoolean(
                        Constants.NIGHT_MODE,
                        false
                    )
                ) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO
            )
        }

    }

}