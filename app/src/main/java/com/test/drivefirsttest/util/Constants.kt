package com.test.drivefirsttest.util

class Constants {
    companion object {

        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
        const val USER_LANGUAGE = "user_language"
        const val LOGIN_STATUS = "login_status"
        const val NIGHT_MODE = "night_mode"

    }
}